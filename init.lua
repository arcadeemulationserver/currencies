currencies = {}

dofile(minetest.get_modpath("currencies") .. "/api.lua")
dofile(minetest.get_modpath("currencies") .. "/utils.lua")
dofile(minetest.get_modpath("currencies") .. "/chatcmdbuilder.lua")
dofile(minetest.get_modpath("currencies") .. "/commands.lua")